#include "glad/glad.h"
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>
#pragma once

namespace
{
	std::string loadFile(const std::string_view filepath)
	{
		std::ifstream ifs(filepath.data(), std::ios::in | std::ios::binary | std::ios::ate);
		if(!ifs.is_open())
		{
			throw std::runtime_error("Could not open " + std::string(filepath));
		}
		std::string data(ifs.tellg(), 0);
		ifs.seekg(0);
		ifs.read(data.data(), data.size());
		return data;
	}
} // namespace

namespace pgl
{
	void APIENTRY MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void*);

	namespace data
	{
		namespace priv
		{
			template <GLenum target>
			class BufferBase
			{
			protected:
				BufferBase() noexcept
				{
					glGenBuffers(1, &buffer);
					bind();
				}

			public:
				BufferBase(BufferBase&) = delete;
				BufferBase(BufferBase&& other) noexcept :
					buffer(other.buffer),
					capacity(other.capacity)
				{
					other.buffer = 0;
				}
				~BufferBase()
				{
					glDeleteBuffers(1, &buffer);
				}
				void bind()
				{
					glBindBuffer(target, buffer);
				}
				template <typename T>
				void bufferData(const std::vector<T>& data, const GLenum usage)
				{
					glNamedBufferData(buffer, data.size() * sizeof(T), data.data(), usage);
				}
				template <typename T, std::size_t N>
				void bufferData(const std::array<T, N>& data, const GLenum usage) //TODO: Generalize this when C++20 comes around with std::span
				{
					glNamedBufferData(buffer, N * sizeof(T), data.data(), usage);
				}
				template <typename T>
				void reserve(const size_t new_size, const GLenum usage)
				{
					glNamedBufferData(buffer, new_size * sizeof(T), NULL, usage);
					capacity = new_size * sizeof(T);
				}
				template <typename T>
				void bufferSubData(const std::vector<T>& data, const GLintptr offset)
				{
					glNamedBufferSubData(buffer, offset, data.size() * sizeof(T), data.data());
				}
				void* mapBuffer(const GLenum accessMode)
				{
					return glMapNamedBuffer(buffer, accessMode);
				}
				void unmapBuffer()
				{
					glUnmapNamedBuffer(buffer);
				}
				size_t getCapacity() const
				{
					return capacity;
				}
				GLuint getRawBufferName() const
				{
					return buffer;
				}

			protected:
				GLuint buffer;

			private:
				size_t capacity = 0;
			};
		} // namespace priv

		class vbo : public priv::BufferBase<GL_ARRAY_BUFFER>
		{
		};

		class ebo : public priv::BufferBase<GL_ELEMENT_ARRAY_BUFFER>
		{
		};

		class ssbo : public priv::BufferBase<GL_SHADER_STORAGE_BUFFER>
		{
		public:
			void bindBufferBase(const GLuint index)
			{
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer);
			}
		};
	} // namespace data

	namespace storage
	{
		namespace priv
		{
			template <GLenum target>
			class BufferBase
			{
			public:
				BufferBase()
				{
					glGenBuffers(1, &buffer);
					bind();
				}
				BufferBase(BufferBase&) = delete;
				BufferBase(BufferBase&& other) noexcept :
					buffer(other.buffer),
					immutable(other.immutable),
					capacity(other.capacity),
					mapPtr(other.mapPtr)
				{
					other.buffer = 0;
					other.mapPtr = nullptr;
				}
				~BufferBase()
				{
					unmapBuffer();
					glDeleteBuffers(1, &buffer);
				}
				void bind()
				{
					glBindBuffer(target, buffer);
				}
				template <class T>
				void bufferData(const std::vector<T>& data, const GLbitfield flags, bool explicit_flush = false)
				{
					if(explicit_flush)
					{
						this->flags = (flags | GL_MAP_FLUSH_EXPLICIT_BIT);
					}
					else
					{
						this->flags = flags;
					}
					unmapBuffer();
					if(immutable)
					{
						glDeleteBuffers(1, &buffer);
						glGenBuffers(1, &buffer);
						bind();
					}
					glNamedBufferStorage(buffer, data.size() * sizeof(T), data.data(), flags);
					capacity = data.size() * sizeof(T);
					immutable = true;
				}
				template <class T, std::size_t N>
				void bufferData(const std::array<T, N>& data, const GLbitfield flags, bool explicit_flush = false)
				{
					if(explicit_flush)
					{
						this->flags = (flags | GL_MAP_FLUSH_EXPLICIT_BIT);
					}
					else
					{
						this->flags = flags;
					}
					unmapBuffer();
					if(immutable)
					{
						glDeleteBuffers(1, &buffer);
						glGenBuffers(1, &buffer);
						bind();
					}
					glNamedBufferStorage(buffer, N* sizeof(T), data.data(), flags);
					capacity = N * sizeof(T);
					immutable = true;
				}
				template <typename T>
				void reserve(const size_t new_size, const GLbitfield flags, bool explicit_flush = false)
				{
					if(explicit_flush)
					{
						this->flags = (flags | GL_MAP_FLUSH_EXPLICIT_BIT);
					}
					else
					{
						this->flags = flags;
					}
					unmapBuffer();
					if(immutable)
					{
						glDeleteBuffers(1, &buffer);
						glGenBuffers(1, &buffer);
						bind();
					}
					glNamedBufferStorage(buffer, new_size * sizeof(T), NULL, flags);
					immutable = true;
					capacity = new_size * sizeof(T);
				}
				void* getMappedPtr()
				{
					if(mapPtr == nullptr)
					{
						mapPtr = glMapNamedBufferRange(buffer, 0, capacity, flags);
					}
					return mapPtr;
				}
				void unmapBuffer()
				{
					if(mapPtr)
					{
						glUnmapNamedBuffer(buffer);
						mapPtr = nullptr;
					}
				}
				template <typename T = unsigned char>
				void flushBuffer(size_t offset = 0)
				{
					glFlushMappedNamedBufferRange(buffer, offset * sizeof(T), capacity - (offset * sizeof(T)));
				}
				template <typename T = unsigned char>
				void flushBuffer(size_t offset, size_t length)
				{
					glFlushMappedNamedBufferRange(buffer, offset * sizeof(T), length);
				}
				size_t getCapacity() const
				{
					return capacity;
				}
				GLuint getRawBufferName() const
				{
					return buffer;
				}

			protected:
				GLuint buffer;

			private:
				bool immutable = false;
				size_t capacity = 0;
				void* mapPtr = nullptr;
				GLbitfield flags;
			};
		} // namespace priv

		class vbo : public priv::BufferBase<GL_ARRAY_BUFFER>
		{
		};

		class ebo : public priv::BufferBase<GL_ELEMENT_ARRAY_BUFFER>
		{
		};

		class ssbo : public priv::BufferBase<GL_SHADER_STORAGE_BUFFER>
		{
		public:
			void bindBufferBase(const GLuint index)
			{
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer);
			}
		};

		class ubo : public priv::BufferBase<GL_UNIFORM_BUFFER>
		{
		public:
			void bindBufferBase(const GLuint index)
			{
				glBindBufferBase(GL_UNIFORM_BUFFER, index, buffer);
			}
		};
	} // namespace storage

	class vao
	{
	public:
		vao()
		{
			glGenVertexArrays(1, &name);
		}
		~vao()
		{
			glDeleteVertexArrays(1, &name);
		}
		void bind()
		{
			glBindVertexArray(name);
		}
		void bindEBO(const storage::ebo& ebo)
		{
			glVertexArrayElementBuffer(name, ebo.getRawBufferName());
		}
		void bindEBO(const data::ebo& ebo)
		{
			glVertexArrayElementBuffer(name, ebo.getRawBufferName());
		}

	private:
		GLuint name;
	};

	class Shader
	{
	public:
		Shader(GLenum shaderType, const std::string_view filepath, const bool binary = false)
		{
			shader = glCreateShader(shaderType);
			if(!filepath.empty())
			{
				if(binary)
				{
					loadBinaryFromFile(filepath);
				}
				else
				{
					loadFromFile(filepath);
				}
			}
		}
		Shader(const Shader&) = delete;
		Shader& operator=(const Shader&) = delete;
		Shader(Shader&& other) : shader(other.shader)
		{
			other.shader = 0;
		}
		Shader& operator=(Shader&& other)
		{
			std::exchange(shader, other.shader);
			return *this;
		}
		~Shader()
		{
			glDeleteShader(shader);
		}
		void loadFromFile(const std::string_view file)
		{
			const std::string source = loadFile(file);
			const GLchar* sourceptr = source.c_str();
			const GLint sourcelenght = source.length();
			glShaderSource(shader, 1, &sourceptr, &sourcelenght);
			glCompileShader(shader);
			GLint logLength;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
			if(logLength > 0)
			{
				std::string shaderlog(logLength, 0);
				glGetShaderInfoLog(shader, logLength, 0, shaderlog.data());
				std::cerr << "Shader compilation log: " << shaderlog << std::endl;
			}
		}
		void loadBinaryFromFile(const std::string_view file)
		{
			const std::string source = loadFile(file);
			const GLchar* sourceptr = source.c_str();
			const GLint sourcelenght = source.length();
			glShaderBinary(1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V_ARB, sourceptr, sourcelenght);
			glSpecializeShaderARB(shader, "main", 0, nullptr, nullptr);
			GLint logLength;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
			if(logLength > 0)
			{
				std::string shaderlog(logLength, 0);
				glGetShaderInfoLog(shader, logLength, 0, shaderlog.data());
				std::cout << "Shader compilation log: " << shaderlog << std::endl;
			}
		}
		GLuint name() const noexcept
		{
			return shader;
		}

	private:
		GLuint shader;
	};

	class Program
	{
		class ProgramTracker
		{
		public:
			ProgramTracker(ProgramTracker const&) = delete;
			void operator=(ProgramTracker const&) = delete;
			static GLuint getCurrent()
			{
				return get().current;
			}
			static void setCurrent(const GLuint value)
			{
				get().current = value;
			}

		private:
			GLuint current = 0;
			ProgramTracker() = default;
			static ProgramTracker& get()
			{
				static ProgramTracker instance;
				return instance;
			}
		};

	public:
		template<typename... Ts, typename = std::enable_if_t<(std::is_same_v<std::decay_t<Ts>, Shader> && ...)>>
		Program(Ts&& ...shaders) : program(glCreateProgram())
		{
			(glAttachShader(program, shaders.name()), ...);
			glLinkProgram(program);
			GLint logLength;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
			if(logLength > 0)
			{
				std::string infoLog(logLength, 0);
				glGetProgramInfoLog(program, logLength, nullptr, infoLog.data());
				std::cerr << "Program linking log: " << infoLog << '\n';
			}
			(glDetachShader(program, shaders.name()), ...);
		}
		Program(Program&) = delete;
		Program(Program&& other) noexcept :
			program(other.program)
		{
			other.program = 0;
		}
		~Program()
		{
			glDeleteProgram(program);
		}
		void use()
		{
			if(ProgramTracker::getCurrent() != program)
			{
				ProgramTracker::setCurrent(program);
				glUseProgram(program);
			}
		}
		template <typename T>
		void setAttrib(const std::string_view attrib, const GLint size, const GLenum type, const GLboolean normalized, const GLsizei stride, const size_t offset)
		{
			GLint loc = getAttrib(attrib);
			glEnableVertexAttribArray(loc);
			switch(type)
			{
				case GL_BYTE:
				case GL_UNSIGNED_BYTE:
				case GL_SHORT:
				case GL_UNSIGNED_SHORT:
				case GL_INT:
				case GL_UNSIGNED_INT:
					glVertexAttribIPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
				case GL_HALF_FLOAT:
				case GL_FLOAT:
				case GL_FIXED:
				case GL_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_10F_11F_11F_REV:
					glVertexAttribPointer(loc, size, type, normalized, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
				case GL_DOUBLE:
					glVertexAttribLPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
			}
		}
		template <typename T>
		void setAttrib(const GLint loc, const GLint size, const GLenum type, const GLboolean normalized, const GLsizei stride, const size_t offset)
		{
			glEnableVertexAttribArray(loc);
			switch(type)
			{
				case GL_BYTE:
				case GL_UNSIGNED_BYTE:
				case GL_SHORT:
				case GL_UNSIGNED_SHORT:
				case GL_INT:
				case GL_UNSIGNED_INT:
					glVertexAttribIPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
				case GL_HALF_FLOAT:
				case GL_FLOAT:
				case GL_FIXED:
				case GL_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_10F_11F_11F_REV:
					glVertexAttribPointer(loc, size, type, normalized, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
				case GL_DOUBLE:
					glVertexAttribLPointer(loc, size, type, stride * sizeof(T), reinterpret_cast<T*>(offset * sizeof(T)));
					break;
			}
		}
		GLint getUniformLocation(const std::string_view name)
		{
			return glGetUniformLocation(program, name.data());
		}

	private:
		std::unordered_map<std::string_view, GLint> attribs;
		GLint getAttrib(const std::string_view attrib)
		{
			if(attribs.find(attrib) == attribs.end())
			{
				const GLint location = glGetAttribLocation(program, attrib.data());
				if(location == -1)
				{
					throw std::runtime_error("Could not get attribute location for \"" + std::string(attrib) + "\" in program " + std::to_string(program));
				}
				attribs.emplace(attrib, location);
			}
			return attribs[attrib];
		}
		GLuint program = 0;
	};

	class Texture
	{
		class TextureTracker
		{
		public:
			TextureTracker(TextureTracker const&) = delete;
			void operator=(TextureTracker const&) = delete;
			static GLuint getCurrent()
			{
				return get().current;
			}
			static void setCurrent(const GLuint value)
			{
				get().current = value;
			}

		private:
			GLuint current = 0;
			TextureTracker() = default;
			static TextureTracker& get()
			{
				static TextureTracker instance;
				return instance;
			}
		};
		GLuint tex;
		bool immutable = false;
		GLsizei curWidth = 0, curHeight = 0;

	public:
		Texture()
		{
			glGenTextures(1, &tex);
		}
		Texture(Texture const&) = delete;
		Texture(Texture&& other) noexcept :
			tex(other.tex)
		{
			other.tex = 0;
		}
		void bind()
		{
			if(TextureTracker::getCurrent() != tex)
			{
				TextureTracker::setCurrent(tex);
				glBindTexture(GL_TEXTURE_2D, tex);
			}
		}
		void load(const GLint internalFormat, const GLsizei width, const GLsizei height, const GLenum format, const GLenum type, const GLvoid* data)
		{
			bind();
			if(width != curWidth || height != curHeight)
			{
				if(immutable)
				{
					glDeleteTextures(1, &tex);
					glGenTextures(1, &tex);
					glBindTexture(GL_TEXTURE_2D, tex);
				}
				glTexStorage2D(GL_TEXTURE_2D, 1, internalFormat, width, height);
				curWidth = width;
				curHeight = height;
			}
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, format, type, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			immutable = true;
		}
		~Texture()
		{
			glDeleteTextures(1, &tex);
		}
	};
} // namespace pgl
