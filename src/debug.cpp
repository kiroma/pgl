#include "pgl.hpp"
#include <string>
#include <iostream>

namespace pgl
{
	void APIENTRY MessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void*)
	{
		std::string msg(message, length);
		std::ostream &ostr = (type == GL_DEBUG_TYPE_ERROR ? std::cerr : std::cout);
		ostr << "gl";
		switch(severity)
		{
			case GL_DEBUG_SEVERITY_HIGH:
				ostr << "ERROR";
				break;
			case GL_DEBUG_SEVERITY_MEDIUM:
				ostr << "Warning";
				break;
			case GL_DEBUG_SEVERITY_LOW:
				ostr << "Alert";
				break;
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				ostr << "Notify";
				break;
			default:
				ostr << "Unknown";
				break;
		}
		ostr << " (" << id << "): ";
		switch(source)
		{
			case GL_DEBUG_SOURCE_API:
				ostr << "API";
				break;
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				ostr << "Window system";
				break;
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				ostr << "Shader compiler";
				break;
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				ostr << "Third party";
				break;
			case GL_DEBUG_SOURCE_APPLICATION:
				ostr << "Application";
				break;
			case GL_DEBUG_SOURCE_OTHER:
				ostr << "Unknown";
				break;
			default:
				ostr << "???";
				break;
		}
		ostr << " reported ";
		switch(type)
		{
			case GL_DEBUG_TYPE_ERROR:
				ostr << "error";
				break;
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				ostr << "deprecated behaviour";
				break;
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				ostr << "undefined behaviour";
				break;
			case GL_DEBUG_TYPE_PORTABILITY:
				ostr << "portability issue";
				break;
			case GL_DEBUG_TYPE_PERFORMANCE:
				ostr << "performance issue";
				break;
			case GL_DEBUG_TYPE_MARKER:
				ostr << "annotation";
				break;
			case GL_DEBUG_TYPE_PUSH_GROUP:
				ostr << "debug group push";
				break;
			case GL_DEBUG_TYPE_POP_GROUP:
				ostr << "debug group pop";
				break;
			case GL_DEBUG_TYPE_OTHER:
			default:
				ostr << "unknown";
				break;
		}
		ostr << ": " << msg << std::endl;
	}
}
